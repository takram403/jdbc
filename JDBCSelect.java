package com.st.jdbc.connection.curd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.st.jdbc.connection.JdbcConnection;

public class JDBCSelect {
	
	public static void main(String[] args) {
		try {
			Connection con = JdbcConnection.getConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from Employee");
			while (rs.next())
				System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
			con.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}


}
