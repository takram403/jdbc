package com.st.jdbc.connection.curd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.st.jdbc.connection.JdbcConnection;

public class JDBCinsert {
	public static void main(String[] args) {
		try {
			Connection con = JdbcConnection.getConnection();
			
			Statement stmt=con.createStatement(); 
			  
			int result=	stmt.executeUpdate("insert into Employee values (105,'Shahan',22,'Medical')");   
			System.out.println(result+" records affected");  
			  
			con.close();   
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
