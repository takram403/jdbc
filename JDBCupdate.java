package com.st.jdbc.connection.curd;

import java.sql.Connection;
import java.sql.Statement;

import com.st.jdbc.connection.JdbcConnection;

public class JDBCupdate {
	public static void main(String[] args) {
		try {
			Connection con = JdbcConnection.getConnection();
			Statement stmt = con.createStatement();
			int result=stmt.executeUpdate("update Employee set EmpName='Taha' where Empid=103");
			System.out.println(result+" records affected");  
			con.close();  
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
